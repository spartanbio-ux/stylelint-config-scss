# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.5](https://gitlab.com/spartanbio-ux/stylelint-config-scss/compare/v1.0.4...v1.0.5) (2019-08-27)

### [1.0.4](https://gitlab.com/spartanbio-ux/stylelint-config-scss/compare/v1.0.3...v1.0.4) (2019-08-26)



### [1.0.3](https://gitlab.com/spartanbio-ux/stylelint-config-scss/compare/v1.0.2...v1.0.3) (2019-06-18)


### Bug Fixes

* adds exceptions for `[@else](https://gitlab.com/else)` rules ([295fb62](https://gitlab.com/spartanbio-ux/stylelint-config-scss/commit/295fb62))



### [1.0.2](https://gitlab.com/spartanbio-ux/stylelint-config-scss/compare/v1.0.1...v1.0.2) (2019-06-17)


### Bug Fixes

* extends config in correct order ([989758b](https://gitlab.com/spartanbio-ux/stylelint-config-scss/commit/989758b))



### [1.0.1](https://gitlab.com/spartanbio-ux/stylelint-config-scss/compare/v1.0.0...v1.0.1) (2019-06-17)


### Bug Fixes

*  corrects instructions ([6d9ce77](https://gitlab.com/spartanbio-ux/stylelint-config-scss/commit/6d9ce77))



## 1.0.0 (2019-06-17)
